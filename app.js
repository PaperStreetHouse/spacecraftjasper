const app = {

    game : {},

    config : {
        type: Phaser.AUTO,
        width: 480,
        height: 544,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 200 }
            }
        }
    },

    current_scene : 'intro',

    loadWorld(world_name) {
        const self = this;
        const scenes = Object.keys(self.game.scene.keys);
        requirejs(['worlds/space/game.dist.js'], function(world) {
            self.game.scene.stop(self.current_scene);
            if(scenes.indexOf(world_name) === -1) {
                self.game.scene.add(world_name, {
                    active: true,
                    width: 384,
                    height: 352,
                    preload: world.preload,
                    create: world.create,
                    update : world.update
                });
            } else {
                self.game.scene.start(world_name);
            }
            self.current_scene = world_name;
        });

    },

    worldPreload() {
        console.log('[Preload] - Should be Overwritten');
    },

    worldCreate() {
        console.log('Should be Overwritten');
    },

    worldUpdate() {

    },

    update() {
        const self = app;
        self.worldUpdate(this);
    },

    create () {
        const self = app;
        self.worldCreate(this);
    },

    preload() {
        const self = app;
        self.worldPreload();
    },

    loadIntro() {
        const self = this;
        self.worldUpdate = function() {};
        self.worldCreate = function() {};
        self.worldPreload = function() {};
        self.game.scene.stop(self.current_scene);
        self.game.scene.start('intro')
    },

    init() {
        const self = this;

        self.config.scene = [{
            active: true,
            renderToTexture: true,
            width: 384,
            height: 352,
            key : 'intro',
            preload: self.preload,
            create: self.create,
            update : self.update
        }];

        self.game = new Phaser.Game(self.config);

        self.loadIntro();
    }
};

app.init();
app.loadWorld('space');