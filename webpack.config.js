const path = require('path');

module.exports = [
    {
        module: {
            rules: [
                {
                    test: /node_modules[/\\]jsonstream/i,
                    use: 'shebang-loader'
                }
            ]
        },
        entry: './public/worlds/space/game.js',
        output: {
            filename: 'game.dist.js',
            path: path.resolve(__dirname, 'public/worlds/space/'),
        },
    },
    {
        module: {
            rules: [
                {
                    test: /node_modules[/\\]jsonstream/i,
                    use: 'shebang-loader'
                }
            ]
        },
        entry: './app.js',
        output: {
            filename: 'app.dist.js',
            path: path.resolve(__dirname, 'public/assets/'),
        },
    },
];