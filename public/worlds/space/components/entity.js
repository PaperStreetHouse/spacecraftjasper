import {Tile} from './tile'

let Entity = function Entity(game, world) {
    this.game = game;
    this.world = world;
};

Entity.prototype = {
    constructor: Entity,
    sprite: undefined,

    id : undefined,
    name : "",
    speed: 0.5,
    control: false,

    max_health : 50,
    health : 50,

    max_mana : 100,
    mana : 100,

    movement_x: 4,
    movement_y: 3,

    scroll_pad : 3,

    attack_range : 1,

    move_frames: 32,
    move_lock: false,

    movement_grid: {},
    grid_tiles: {},

    health_text : {},

    direction_map: {
        right: {
            direction: 1,
            axis: 'x'
        },
        left: {
            direction: -1,
            axis: 'x'
        },

        up: {
            direction: -1,
            axis: 'y'
        },

        down: {
            direction: 1,
            axis: 'y'
        }

    },

    abilities : {},

    updateHealthText() {
        const self = this;
        if(self.health_text.destroy === undefined) {
            self.health_text = self.game.add.text(self.sprite.x - 8, self.sprite.y - 36, self.health, { font: "bold 16px Arial", fill: '#FFFFFF'})
        }
        self.health_text.x = self.sprite.x - 8;
        self.health_text.y = self.sprite.y - 36;
        self.health_text.text = "" + self.health;
        self.health_text.depth = 5;
        self.health_text.setShadow(4, 4, 'rgba(0,0,0,1)', 2)
    },

    myTile() {
        return this.getTile(this.sprite.x, this.sprite.y);
    },

    eraseGrid() {
        const self = this;
        for (let x in self.grid_tiles) {
            for (let y in self.grid_tiles[x]) {
                if (self.grid_tiles[x][y] !== undefined) {
                    self.grid_tiles[x][y].destroy();
                }
            }
        }
        self.grid_tiles = {};
    },

    moveToTile(tile_x, tile_y, sprite) {
        const self = this;

        if(sprite === undefined) {
            sprite = self.sprite;
        }

        const actual_x = (tile_x * 32) + 16;
        const actual_y = (tile_y * 32) + 16;

        sprite.x = actual_x;
        sprite.y = actual_y;

        return {
            x : actual_x,
            y : actual_y
        };
    },

    drawGrid() {
        const self = this;
        self.movement_grid = {};

        let tile = this.myTile();

        // Determine the top left coordinate for the start of the grid
        let left = self.movement_x * -1;
        let width = left + ((self.movement_x * 2) + 1);

        let top = self.movement_y * -1;
        let height = top + ((self.movement_y * 2) + 1);

        self.eraseGrid(); // Remove the old grid
        let bottom_tile = self.getTile(self.world.map_width, self.world.map_height);

        const map = self.world.map;

        for (let x = left; x < width; x++) {
            for (let y = top; y < height; y++) {
                let passable = true;

                // Check the map file to see if the tile is passable
                if (map[tile[0] + x] !== undefined && map[tile[0] + x][tile[1] + y] !== undefined) {
                    if (!map[tile[0] + x][tile[1] + y].passable) {
                        passable = false;
                    }
                }

                // console.log(tile [0] + x, bottom_tile[0]);
                if(tile[0] + x < 0 || (tile [0] + x) >= bottom_tile[0]) {
                    passable = false;
                }

                if (passable) {
                    let color = 'blue';
                    const new_x = tile[0] + x;
                    const new_y = tile[1] + y;

                    for(let key in self.game.entities) {
                        let entity = self.game.entities[key];
                        const e_tile = entity.myTile();

                        let my_tile = new_x === tile[0] && new_y === tile[1];

                        if(self.world.map[e_tile[0]][e_tile[1]] === undefined) {
                            self.world.map[e_tile[0]][e_tile[1]] = {
                                passable : true
                            }
                        }
                        self.world.map[e_tile[0]][e_tile[1]].passable = true;

                        if(e_tile[1] === new_y && e_tile[0] === new_x && !my_tile) {
                            color = 'red';
                            self.world.map[e_tile[0]][e_tile[1]].passable = false;
                        }
                    }
                    let new_tile = Tile.get(this.game, new_x, new_y, color, 0.75);

                    let tile_x = new_tile.tile[0];
                    let tile_y = new_tile.tile[1];
                    if(tile_y == 5) {
                        console.log("FOUND IT")
                    }

                    if (self.movement_grid[tile_x] === undefined) {
                        self.movement_grid[tile_x] = {};
                    }

                    if (self.grid_tiles[tile_x] === undefined) {
                        self.grid_tiles[tile_x] = {};
                    }

                    self.movement_grid[tile_x][tile_y] = passable;
                    self.grid_tiles[tile_x][tile_y] = new_tile.graphics;

                }


            }
        }

        // Clean up grid based on pathing
        for (let x in self.grid_tiles) {
            for (let y in self.grid_tiles[x]) {
                let path = self.getEntityPathTo(x, y);
                if (!path && self.grid_tiles[x][y] !== undefined) {
                    self.grid_tiles[x][y].destroy();
                    if (self.movement_grid[x] !== undefined) {
                        self.movement_grid[x][y] = false;
                    }
                }
            }
        }


    },


    assumeControl() {
        const self = this;
        for (let i in self.game.entities) {
            let control = this.game.entities[i].control;
            this.game.entities[i].control = false;

            if (control) {
                this.game.entities[i].eraseGrid();
            }
        }
        this.control = true;
        this.drawGrid();
    },

    getTile(x, y) {
        const self = this;
        return self.world.getTile(x, y)
    },

    getDirectionOffset(direction) {
        const self = this;
        let addition = {
            x: 0,
            y: 0
        };

        if (self.direction_map[direction] !== undefined) {
            addition[self.direction_map[direction].axis] = self.direction_map[direction].direction * 32;
        }

        return addition;
    },

    taking_turn : false,
    takeTurn() {
        const self = this;
        if(self.taking_turn) {
            return false;
        }
        self.taking_turn = true;
        let enemy;

        // Scan for enemy
        for(let i in self.world.entities) {
            if(self.world.entities[i].player == true) {
                // Enemy Acquired
                enemy = self.world.entities[i];
            }
        }

        // Get enemy tile
        let enemy_tile = enemy.myTile();
        // Get my tile
        const my_tile = self.myTile();

        enemy.eraseGrid();
        self.eraseGrid();
        self.drawGrid();

        const then = (new Date).getTime();

        if(Math.abs(enemy_tile[0] - my_tile[0]) != 1) {
            enemy_tile[0] += enemy_tile[0] / Math.abs(enemy_tile[0]);
        } else {
            enemy_tile[0] = my_tile[0];
        }

        let path = self.findPath(my_tile, self.movement_grid, enemy_tile);
        let now = (new Date).getTime();


        console.log("Path generated in " + (now - then) + " milliseconds");

        self.locked = true;

        self.eraseGrid();
        let count = 0
        if(enemy_tile[0] != my_tile[0]) {
            this.followPath(path, count, function() {
                self.attack(self.world.entities[enemy.id]);
                self.eraseGrid();
                enemy.drawGrid();
            });
        } else {
            self.attack(self.world.entities[enemy.id]);
            self.eraseGrid();
            enemy.drawGrid();
        }
        // self.world.turn = 'player';



    },

    autoMove(directions, count) {
        const self = this;
        entity.move(directions[count]).then(function () {
            if (count !== (directions.length - 1)) {
                self.autoMove(entity, directions, count + 1)
            }
        })
    },

    move(direction) {
        const self = this;
        const direction_offset = self.getDirectionOffset(direction);

        // Check if we're even allowed to move to that tile
        let new_x = self.sprite.x + direction_offset.x;
        let new_y = self.sprite.y + direction_offset.y;

        const new_tile = self.getTile(new_x, new_y);
        let resolve_func;
        let deferred = new Promise(function (resolve) {
            resolve_func = resolve;
        });

        for (let x in self.movement_grid) {
            for (let y in self.movement_grid[x]) {
                if (x == new_tile[0] && y == new_tile[1] && self.movement_grid[x][y]) {
                    let moved = 0;
                    this.move_lock = true;

                    let move_id = window.setInterval(function () {

                        let current_tile = self.getTile(self.sprite.x, self.sprite.y);
                        let bottom_tile = self.getTile(self.world.map_width, self.world.map_height);

                        let move_frames = self.move_frames;
                        let speed = self.speed;
                        moved += speed;

                        if (moved > self.move_frames) {
                            speed = speed - (moved - self.move_frames);
                        }

                        switch (direction) {
                            case 'right':

                                if (self.sprite.anims.currentAnim.key !== 'horizontal') {
                                    self.sprite.anims.play('horizontal');
                                }
                                self.sprite.scaleX = -1;

                                self.sprite.x += speed;
                                if (self.control && new_tile[0] >= self.scroll_pad) {
                                    self.game.cameras.main.scrollX += speed;
                                }
                                break;

                            case 'left':
                                if (self.sprite.anims.currentAnim.key !== 'horizontal') {
                                    self.sprite.anims.play('horizontal');
                                }
                                self.sprite.scaleX = 1;

                                self.sprite.x -= speed;
                                if (self.control && new_tile[0] < (bottom_tile[0] - self.scroll_pad)) {
                                    self.game.cameras.main.scrollX -= speed;
                                }
                                break;

                            case 'up':
                                self.sprite.y -= speed;
                                if (self.sprite.anims.currentAnim.key !== 'walk_up') {
                                    self.sprite.anims.play('walk_up');
                                }

                                if (self.control && new_tile[1] < (bottom_tile[1] - self.scroll_pad)) {
                                    self.game.cameras.main.scrollY -= speed;
                                }
                                break;

                            case 'down':
                                if (self.sprite.anims.currentAnim.key !== 'stand') {
                                    self.sprite.anims.play('stand');
                                }

                                self.sprite.y += speed;
                                if (self.control && new_tile[1] >= self.scroll_pad) {
                                    self.game.cameras.main.scrollY += speed;
                                }
                                break;
                        }

                        if (moved >= move_frames) {
                            window.clearInterval(move_id);
                            self.move_lock = false;
                            // self.stand();
                            resolve_func()
                        }


                    }, 10);
                    break;
                }
            }
        }

        return deferred;
    },

    stand() {
        if (this.sprite.anims.currentAnim.key !== 'stand') {
            this.sprite.anims.play('stand');
        }
    },

    setAnimations(setFunc) {
        setFunc.bind(this);
        setFunc();
    },

    setSprite(key, image, coords) {

        if (coords === undefined) {
            coords = [5, 5];
        }

        this.sprite = this.game.add.sprite((coords[0] * 32) - 16, (coords[1] * 32) - 16, key, image);
        this.sprite.setScale(1, 1);
        this.sprite.depth = 1;
    },

    load(loadFunc, game) {
        loadFunc(game);
    },

    getEntityPathTo(x, y) {
        let entity_tile = this.getTile(this.sprite.x, this.sprite.y);
        return this.findPath(entity_tile, this.movement_grid, [x, y]);
    },

    findPath(start, grid, goal) {
        let start_x = start[0];
        let start_y = start[1];

        var start_location = {
            x: start_x,
            y: start_y,
            path: [],
            status: 'start'
        };

        // console.log(grid);
        // return false;

        let queue = [start_location];

        // Loop through the grid searching for the goal
        let tries = 0;
        while (queue.length > 0) {
            if (tries > 6000) {
                break;
            }
            tries++;
            // Take the first location off the queue
            const currentLocation = queue.shift();

            let new_location;
            for (let direction in this.direction_map) {
                new_location = this.exploreInDirection(currentLocation, direction, grid, goal);
                if (new_location.status === 'found') {
                    return new_location.path;
                } else if (new_location.status === 'valid') {
                    queue.push(new_location);
                }
            }
        }

        // No valid path found
        return false;
    },

    // This function will check a location's status
    // (a location is "valid" if it is on the grid, is not an "obstacle",
    // and has not yet been visited by our algorithm)
    // Returns "Valid", "Invalid", "Blocked", or "Goal"
    locationStatus(location, grid, goal) {
        const x = location.x;
        const y = location.y;

        if (grid[x] === undefined || (grid[x] !== undefined && grid[x][y] === undefined)) {
            // location is not on the grid--return false
            return 'invalid';
        } else if (x == goal[0] && y == goal[1]) {
            return 'found';
        } else if (grid[x][y] === false) {
            // location is either an obstacle or has been visited
            return 'blocked';
        } else {
            return 'valid';
        }
    },

    // Explores the grid from the given location in the given
    // direction
    exploreInDirection(currentLocation, direction, grid, goal) {
        let newPath = currentLocation.path.slice();
        newPath.push(direction);

        let directions = {
            x: currentLocation.x,
            y: currentLocation.y
        };

        const axis = this.direction_map[direction].axis;
        directions[axis] += this.direction_map[direction].direction;

        var new_location = {
            x: directions.x,
            y: directions.y,
            path: newPath,
            status: true
        };
        new_location.status = this.locationStatus(new_location, grid, goal);

        // If this new location is valid, mark it as 'Visited'
        if (new_location.status === 'valid') {
            grid[new_location.x][new_location.y] = 'visited';
        }

        return new_location;
    },

    doneMoving(callback) {
        const self = this;
        self.locked = false;
        self.drawGrid();
        self.stand();
        if(callback) {
            callback();
        }
    },

    followPath(directions, count, callback) {
        const self = this;

        if(callback === undefined) {
            callback = function(){}
        }

        self.move(directions[count]).then(function () {
            if (count !== (directions.length - 1)) {
                self.followPath(directions, count + 1, callback);
            } else {
                self.doneMoving(callback);
            }
        })
    },

    checkTileForEntity(tile) {
        let tile_entity = false;
        for(let key in this.game.entities) {
            let entity = this.game.entities[key];
            const e_tile = entity.myTile();
            if(this.assertTiles(e_tile, tile)) {
                tile_entity = entity;
                break;
            }
        }

        return tile_entity;
    },

    assertTiles(one, two) {
        const self = this;
        return self.world.assertTiles(one, two);
    },

    endTurn() {
        const self = this;

        self.world.turn += 1;
        if(self.world.turn >= self.world.entities.length) {
            self.world.turn = 0;
        }
        self.taking_turn = false;
    },

    attack(target_entity) {
        const self = this;
        const their_tile = target_entity.myTile();
        self.world.globalAnims.moveToTile(their_tile[0], their_tile[1]);
        const anim_reference = self.world.globalAnims.sprite.play('cut_a')
        console.log(anim_reference.anims);
        const interval = window.setInterval(function() {
            if(!anim_reference.anims.isPlaying) {
                window.clearInterval(interval);
                const spell = self.world.SPELLS[self.world.charged_spell];
                if(spell.mana) {
                    if(spell.mana > self.mana) {
                        return false;
                    }
                    self.world.entities[self.id].mana -= spell.mana;
                }
                let new_health = target_entity.health - (spell.damage[1] - Math.floor(spell.damage[0] * Math.random()));

                if(new_health < 0) {
                    new_health = 0;
                }

                self.world.entities[target_entity.id].health = new_health;

                console.log("New Health For "+target_entity.name+": ", target_entity.health)
                self.world.entities[target_entity.id].updateHealthText();
                self.endTurn();
            }
        }, 5);
    },

    locked: false,
    handleClick() {
        const self = this;
        if (this.locked) {
            return false;
        }

        this.locked = true;

        let x = this.pointer.downX;
        let y = this.pointer.downY;

        this.scrollOffset = [this.game.cameras.main.scrollX, this.game.cameras.main.scrollY];

        const clicked_tile = this.getTile(x + this.scrollOffset[0], y + this.scrollOffset[1]);
        const start_tile = this.getTile(this.sprite.x, this.sprite.y);

        const then = (new Date).getTime();

        const path = this.findPath(start_tile, this.movement_grid, clicked_tile);
        let now = (new Date).getTime();

        console.log("Path generated in " + (now - then) + " milliseconds");

        if(!path) {
            this.locked = false;
            return false;
        }

        const tile_entity = this.checkTileForEntity(clicked_tile);
        // console.log('Tile Entity', tile_entity);
        if(tile_entity !== false) {
            // console.log('Clicked on me!', tile_entity);
            //tile_entity.assumeControl();

            if(!tile_entity.control) {
                self.attack(tile_entity);
            }

            this.locked = false;


            return true;
        }

        let count = 0;
        this.followPath(path, count);
    },

    handleInput() {
        if (this.control === true) {

            if (this.pointer.justDown) {
                this.handleClick();
            }

            // for (let direction in this.direction_map) {
            //
            //     if (this.cursors[direction].isDown && !this.move_lock) {
            //         this.move(direction);
            //     }
            // }

        }
    }

};

export {Entity}