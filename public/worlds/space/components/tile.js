let Tile = {
    get(game, x, y, color, alpha) {
        if (color === undefined) {
            color = 0x0000FF;
        }

        if (alpha === undefined) {
            alpha = 0.1;
        }

        const rect = new Phaser.Geom.Rectangle(x * 32, y * 32, 32, 32);
        // const graphics = game.add.graphics({fillStyle: {color: color}});
        let graphics = game.add.sprite((x * 32) + 16, (y * 32) + 16, 'grid_tile_' + color);

        console.log(color);
        graphics.alpha = alpha;
        // graphics.fillRectShape(rect);
        graphics.depth = 0;

        return {
            tile: [x, y],
            graphics: graphics
        };
    }
}

export {Tile}