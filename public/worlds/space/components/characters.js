import {Entity} from "./entity";

const Titus = {
    name : "Titus",
    control : true,
    player : true,
    start_coords : {
        x: 6,
        y: 21
    },
    abilities : {
        slash : {
            damage : [5, 10]
        },
        fireball : {
            mana : 15,
            damage : [15, 30]
        }
    },
    animations : {
        walk_up : {
            start: 1,
            end: 4,
            prefix : 'up_',
        },
        horizontal : {
            start : 1,
            end : 4,
            prefix : 'left_',
        },
        stand : {
            start : 1,
            end : 4,
            prefix : 'down_',


        }
    }
}

const Sephiroth = {
    name : "Sephiroth",
    start_coords : {
        x: 9,
        y: 21
    },
    animations : {
        walk_up : {
            start: 1,
            end: 4,
            prefix : 'up_',
        },
        horizontal : {
            start : 1,
            end : 4,
            prefix : 'left_',
        },
        stand : {
            start : 1,
            end : 4,
            prefix : 'down_',


        }
    }
}
const Reno = {
    name : "Reno",
    start_coords : {
        x: 9,
        y: 19
    },
    animations : {
        walk_up : {
            start: 1,
            end: 4,
            prefix : 'up_',
        },
        horizontal : {
            start : 1,
            end : 4,
            prefix : 'left_',
        },
        stand : {
            start : 1,
            end : 4,
            prefix : 'down_',


        }
    }
}

// const Characters = [Titus, Sephiroth]

const Characters = {

    entities : [Titus, Sephiroth, Reno],

    initialize(game) {
        const self = this;
        const entities = self.entities;
        const entity_list = [];

        for(let ind in entities) {
            let entity = entities[ind];

            const ent = new Entity(game.engine, game);
            ent.id = ind;
            ent.name = entity.name;
            ent.player = entity.player;

            if(entity.control) {
                ent.control = true;
            }

            ent.setAnimations(function () {
                for(let i in entity.animations) {
                    let animation = entity.animations[i];

                    const frames = this.anims.generateFrameNames('character', {
                        start: animation.start, end: animation.end,
                        prefix: animation.prefix, suffix: '.png'
                    });
                    this.anims.create({key: i, frames: frames, frameRate: 7, repeat: -1});

                }

            }.bind(game.engine));
            ent.setSprite('character', '1.png', [entity.start_coords.x, entity.start_coords.y]);
            ent.load(function (game) {
                this.sprite.anims.play('stand');
                this.pointer = game.input.activePointer;
                this.cursors = game.input.keyboard.createCursorKeys();
            }.bind(ent), game.engine);

            entity_list.push(ent);

        }

        return entity_list;

    }
}

export {Characters};