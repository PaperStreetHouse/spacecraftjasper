const Controls = {
    toggleEditMode(event) {
        const game = this;
        if(game.edit_mode) {
            game.engine.debug_text.setText('');
            game.edit_mode = false;
            game.clearGameGrid();
        } else {
            game.edit_mode = true;
            game.drawGameGrid(game.engine);
            console.log(JSON.stringify(game.map));
        }
    }
}

export {Controls}