import {Tile} from './components/tile'
import {Entity} from "./components/entity";
import {Characters} from "./components/characters";
import {Controls} from "./components/controls"
String.prototype.ucwords = function () {
    str = this.toLowerCase();
    return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
        function (s) {
            return s.toUpperCase();
        });
};

let map = {
    "0": {
        "0": {
            "passable": false
        },
        "1": {
            "passable": false
        },
        "2": {
            "passable": false
        },
        "3": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "21": {
            "passable": false
        },
        "22": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "1": {
        "3": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "22": {
            "passable": true
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "2": {
        "3": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "7": {
            "passable": false
        },
        "8": {
            "passable": false
        },
        "9": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "21": {
            "passable": false
        },
        "22": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "3": {
        "3": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "21": {
            "passable": false
        },
        "22": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "4": {
        "3": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "18": {
            "passable": false
        },
        "19": {
            "passable": false
        },
        "20": {
            "passable": false
        },
        "21": {
            "passable": false
        },
        "22": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "5": {
        "3": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "6": {
        "0": {
            "passable": false
        },
        "1": {
            "passable": false
        },
        "2": {
            "passable": false
        },
        "3": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "7": {
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "8": {
        "2": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "7": {
            "passable": false
        },
        "8": {
            "passable": false
        },
        "9": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "9": {
        "6": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "18": {
            "passable": false
        },
        "21": {
            "passable": false
        },
        "22": {
            "passable": false
        },
        "23": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "10": {
        "0": {
            "passable": false
        },
        "1": {
            "passable": false
        },
        "2": {
            "passable": false
        },
        "3": {
            "passable": false
        },
        "5": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        }
    },
    "11": {
        "0": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "15": {
            "passable": false
        },
        "20": {
            "passable": false
        }
    },
    "12": {
        "0": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "7": {
            "passable": false
        },
        "8": {
            "passable": false
        },
        "9": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        }
    },
    "13": {
        "0": {
            "passable": false
        },
        "1": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "17": {
            "passable": false
        },
        "18": {
            "passable": false
        },
        "21": {
            "passable": false
        },
        "22": {
            "passable": false
        },
        "23": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "14": {
        "0": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "15": {
        "0": {
            "passable": false
        },
        "1": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        },
        "24": {
            "passable": false
        },
        "25": {
            "passable": false
        }
    },
    "16": {
        "0": {
            "passable": false
        },
        "6": {
            "passable": false
        },
        "10": {
            "passable": false
        },
        "12": {
            "passable": false
        },
        "13": {
            "passable": false
        },
        "16": {
            "passable": false
        }
    }
};

const menu = {
    destroyMenu() {
        const self = this;
        if(self.menu !== null && self.menu.destroy !== undefined) {
            self.menu.destroy();
        }
    },

    drawMenu(game) {
        const self = this;

        // self.destroyMenu();

        const rect = new Phaser.Geom.Rectangle(0, 0, 200,200);

        let graphics;

        if(!self.menu) {
            graphics = game.add.graphics({fillStyle: {color: 0xFFFFFF}});

        } else {
            graphics = self.menu;
        }

        graphics.alpha = 0.75;
        graphics.fillRectShape(rect);
        graphics.depth = 0;

        self.menu = graphics;
    },
}

const game = {

    character_speed: 0.8,
    entities: [],
    uientities : {},
    activeEntity: {},

    edit_mode : false,

    map_width: 0,
    map_height: 0,
    map : undefined,
    gameTiles : [],

    globalAnims : '',

    charged_spell : 'slash',

    engine : undefined,
    turn : 0,
    taken_turn : {
        0 : false,
        1 : false,
        2 : false
    },

    preload() {
        this.load.atlas('character', 'assets/sprites/character.png', 'assets/sprites/character.json');
        this.load.image('level_1', 'worlds/space/assets/level1.png');
        this.load.image('grid_tile_blue', 'worlds/space/assets/grid_tile_blue.png');
        this.load.image('grid_tile_green', 'worlds/space/assets/grid_tile_green.png');
        this.load.image('grid_tile_red', 'worlds/space/assets/grid_tile_red.png');
        this.load.atlas('cut_a', 'assets/sprites/cut_a.png', 'assets/sprites/cut_a.json');
    },

    create() {
        const self = game;

        // Assigns the collision map to the game
        self.map = map;

        // Assigns the game engine context to the game
        // Things like the graphics, camera, and text objects can be accessed from this variable
        self.engine = this;

        // Initialize the background for the map and set it's origin to the top left corner
        this.add.image(0, 0, 'level_1').setOrigin(0, 0);

        // Initialize the core UI visible at the bottom of the screen
        self.drawUi.bind(self.engine)();
        self.initializeDebugText();

        // This gets the total width and height of the background image
        // We use this to initialize the map, which means the map is directly tied to the dimensions of the background
        // image loaded above
        self.map_height = self.engine.textures.get('level_1').source[0].height;
        self.map_width = self.engine.textures.get('level_1').source[0].width;

        // Bind all keyboard events here
        self.handleKeyDown('CTRL', Controls.toggleEditMode)

        // Initialize Global or Shared Animations, this is for all animations not tied to an entity. Like map events
        // such as falling debris or environmental changes
        const GlobalAnimations = new Entity(this, game);
        GlobalAnimations.setAnimations(function() {
            const cut_a_frames = this.anims.generateFrameNames('cut_a', {
                start: 1, end: 5,
                prefix: 'cut_a_000', suffix: '.png',
            });
            this.anims.create({key: 'cut_a', frames: cut_a_frames, frameRate: 10, repeat: 0});
        }.bind(this));

        GlobalAnimations.setSprite('cut_a', '1.png');
        GlobalAnimations.load(function (game) {
            this.sprite.depth = 4;
            this.sprite.setDisplaySize(32, 32);
        }.bind(GlobalAnimations), this);

        self.globalAnims = GlobalAnimations;

        // Initialize all characters and NPCs as entities with sprites and animations



        this.cameras.main.setBounds(0, 0, 544, 896);
        this.cameras.main.height = 480;
        this.cameras.main.width = 480;

        this.cameras.main.scrollY = 896;

        let count = 0;
        self.entities = Characters.initialize(self);
        self.engine.entities = self.entities

        self.entities[0].drawGrid();
        self.activeEntity = self.entities[0];
        return false;
    },

    update() {
        const self = game;

        if(self.edit_mode) {
            let pointer = this.input.activePointer;
            self.handleEditMode(pointer);
        } else {

            const character = self.entities[self.turn];
            // console.log(character, self.turn);
            if(character.player == true) {
                character.handleInput();
                character.updateHealthText();
            } else {
                character.takeTurn();
            }

            // for (const i in self.entities) {
            //     const entity = self.entities[i];
            //     const taken_turn = self.taken_turn[i];
            //     if(taken_turn) {
            //         continue;
            //     }
            //     if(self.turn != 'player' && entity.control == false) {
            //         entity.takeTurn();
            //     } else {
            //         entity.handleInput();
            //
            //         // if(entity.control) {
            //             entity.updateHealthText();
            //         // }
            //     }
            // }
        }
    },

    getTile(x, y) {
        const tile_x = Math.floor((x) / 32);
        const tile_y = Math.floor((y) / 32);

        return [tile_x, tile_y];
    },

    assertTiles(one, two) {
        return one[0] === two[0] && one[1] === two[1];
    },

    getTileInfo(tile) {
        const self = game;

        let world_tile = self.getTile((tile[0] * 32) + self.engine.cameras.main.scrollX, (tile[1] * 32) + self.engine.cameras.main.scrollY);

        let msg = [];
        let is_control = 'no';
        let entity_in_tile = 'no';
        for(let i in self.entities) {
            const entity = self.entities[i];
            if(self.assertTiles(entity.myTile(), world_tile)) {
                entity_in_tile = 'yes';
                if(entity.control) {
                    is_control = 'yes';
                }
            }
        }

        msg.push('Screen Coordinates : ' + '('+tile[0]+', '+tile[1]+')');
        msg.push('World Coordinates : ' + '('+world_tile[0]+', '+world_tile[1]+')');
        msg.push('Entity in Tile : ' + entity_in_tile);
        msg.push('Entity Controlled : ' + is_control);

        self.engine.debug_text.setText(msg.join('\n'));
        self.engine.debug_text.x = 32 + self.engine.cameras.main.scrollX;
        self.engine.debug_text.y = 32 + self.engine.cameras.main.scrollY;

    },

    addTileCollisionToMap(x, y) {
        const self = game;
        let scrollOffset = [self.engine.cameras.main.scrollX, self.engine.cameras.main.scrollY];

        const clicked_tile = self.getTile(x + scrollOffset[0], y + scrollOffset[1]);
        const new_tile = Tile.get(self.engine, clicked_tile[0], clicked_tile[1], 'red', 0.6);

        x = parseInt(clicked_tile[0]);
        y = parseInt(clicked_tile[1]);

        if(map[x] === undefined) {
            map[x] = {};
        }

        if(map[x][y] === undefined) {
            map[x][y] = {};
        }

        map[x][y].passable = false;
        console.log(JSON.stringify(map, null, 2));
        self.gameTiles.push(new_tile.graphics)
    },

    handleEditMode(pointer) {
        const self = game;
        const tile = self.getTile(pointer.x, pointer.y);
        self.getTileInfo.bind(this)(tile);
        if (pointer.justDown) {
            let x = pointer.downX;
            let y = pointer.downY;
            self.addTileCollisionToMap(x, y)
        }
    },

    handleKeyDown(key, callback) {
        const self = this;
        self.engine.input.keyboard.on('keydown_' + key, callback.bind(self));
    },
    health_bar : undefined,
    mana_bar : undefined,

    drawBar(name, x, y, width, max_val, current_val, color) {
        const self = this;

        if(color === undefined) {
            color = 0xFF0000;
        }

        const outline = new Phaser.Geom.Rectangle(x, y, width, 10);
        const bar = new Phaser.Geom.Rectangle(x, y, width * (current_val / max_val), 10);
        let graphics;
        let bar_key = name + '_bar';
        if(!game[bar_key]) {
            graphics = this.add.graphics({lineStyle: {lineWidth : 2, color : color}, fillStyle: {color: color}});
        } else {
            graphics = game[bar_key];
        }
        // graphics.alpha = 0.75;
        graphics.clear();
        graphics.strokeRectShape(outline);
        graphics.fillRectShape(bar);
        graphics.depth = 0;

        game[bar_key] = graphics;

        // self.health_bar = graphics;
        return graphics;
    },

    SPELLS : {
        fireball : {
            name : 'Fireball',
            description : 'Launch a ball of elemental fire at the enemy',
            damage : [5, 10],
            mana : 20,
        },
        slash : {
            name : 'Slash',
            description : 'Slash with your sword at the enemy',
            damage : [2, 7],
        }
    },

    drawText(name, x, y, message) {
        const self = this;
        let message_key = name + '_text';
        let text;
        if(!game[message_key]) {
            text = this.add.text(x, y, message, { fontSize: '12px', fill: '#FFF' });
        } else {
            text = game[message_key];
        }

        text.setText(message);
        text.x = x;
        text.y = y;
        game[message_key] = text;
    },


    drawUi() {
        const self = game;
        this.scene.add('ui', {
            active: true,
            width: 480,
            height: 544,
            preload: function() {
                this.load.image('fireball', 'worlds/space/assets/fireball.png');
                this.load.image('sword', 'worlds/space/assets/sword.png');
                // this.load.image('grid_tile_blue', 'worlds/space/assets/grid_tile_blue.png');
                // this.load.image('grid_tile_red', 'worlds/space/assets/grid_tile_red.png');
            },
            create: function() {
                const fireball = new Entity(this, game);
                fireball.setSprite('fireball', 'fireball.png');
                fireball.load(function (game) {
                    this.pointer = game.input.activePointer;
                    this.cursors = game.input.keyboard.createCursorKeys();
                }.bind(fireball), this);
                fireball.moveToTile(14, 15);

                const attack = new Entity(this, game);
                attack.setSprite('sword', 'sword.png');
                attack.load(function (game) {
                    this.pointer = game.input.activePointer;
                    this.cursors = game.input.keyboard.createCursorKeys();
                }.bind(attack), this);
                attack.moveToTile(13, 15);

                self.uientities.fireball = fireball;
                self.uientities.attack = attack;
            },

            update : function() {
                let pointer = this.input.activePointer;
                if (pointer.justDown) {
                    const x = pointer.downX;
                    const y = pointer.downY;

                    const tile = self.getTile(x, y);

                    if(self.assertTiles(tile, self.uientities.fireball.myTile())){
                        game.charged_spell = 'fireball';
                        console.log('Charge a Spell!')
                    } else if(self.assertTiles(tile, self.uientities.attack.myTile())) {
                        game.charged_spell = 'slash';
                        console.log('Attack!')
                    }
                }
                // console.log(self.entities[0].health);
                self.drawBar.bind(this)('health', 10, (15 * 32) + 10, 200, self.entities[0].max_health, self.entities[0].health, 0xD33E43);
                self.drawBar.bind(this)('mana', 10, (16 * 32), 200, self.entities[0].max_mana, self.entities[0].mana, 0x427CB4);


                self.drawText.bind(this)('current_spell', (7 * 32) + 10, (15*32) + 10, 'Current Spell : ' + self.SPELLS[game.charged_spell].name);
                self.drawText.bind(this)('current_spell_damage', (7 * 32) + 10, (16*32), 'Damage : ' + self.SPELLS[game.charged_spell].damage.join(' - '));

            }
        });
    },

    initializeDebugText() {
        const self = this;
        self.engine.debug_text = self.engine.add.text(32, 32, '', { fontSize: '16px', fill: '#FFF' });
        self.engine.debug_text.depth = 5;
    },

    clearGameGrid() {
        const self = game;
        for(let key in self.gameTiles) {
            self.gameTiles[key].destroy()
        }
    },
    drawGameGrid(c) {
        const self = game;
        let color = 'green';

        self.clearGameGrid()
        for (let x = 0; x < 50; x++) {
            // if (color === 'red') {
            //     color = 'green';
            // } else {
            //     color = 'red';
            // }
            for (let y = 0; y < 50; y++) {
                // if (color === 'red') {
                //     color = 'green';
                // } else {
                //     color = 'red';
                // }
                let alpha = 0.75;
                let old_color = false;
                if(map[x] !== undefined && map[x][y] !== undefined && map[x][y].passable === false) {
                    old_color = color;
                    color = 'red';
                    alpha = 0.75;
                }
                self.gameTiles.push(Tile.get(c, x, y, color, alpha).graphics);

                if(old_color) {
                    color = old_color;
                }
            }
        }
    }
};

function force(entity, directions, count) {
    entity.move(directions[count]).then(function () {
        if (count !== (directions.length - 1)) {
            force(entity, directions, count + 1)
        }
    })
}

define(game);